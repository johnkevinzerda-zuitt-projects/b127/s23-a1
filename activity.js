//Create a single room
/*
db.singleRoom.insertOne(
{"name":"single",
"accomodates":"2",
"price":"1000",
"description":"a simple room with all the basic necessities"}
)
*/

//update singleRooms
/*
db.singleRooms.updateOne({"_id":ObjectId("613601f70d94c68a411d63d6")},
    {$set:{
    "rooms_available":"10",
    "isAvailable":false
        }
     }
)
*/

//Creates multipleRooms
/*
db.multipleRooms.insertMany([
{"name":"queen",
"accomodates":"4",
"price":"4000",
"description":"a room with a queen sized bed perfect for a simple getaway",
"rooms_available":"15",
"isAvailable":false},
{"name":"double",
"accomodates":"3",
"price":"2000",
"description":"a room fit for asmall family going on a vacation",
"rooms_available":"5",
"isAvailable":false}
])
*/
//Find Rooms
/*
db.multipleRooms.find({"name":"double"})
*/

//Update multipleRooms
/*
db.multipleRooms.updateOne({"name":"queen"},{$set:{"rooms_available":"0"}})
*/

//Use to delete rooms with o availability
/*
db.multipleRooms.deleteMany({"rooms_available":"0"})
*/